//
//  main.m
//  stv_inspectionApp
//
//  Created by Dhantha Gunarathna on 6/9/14.
//  Copyright (c) 2014 stv. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
